﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads_1
{
    class Program
    {
        //this sleep duration will indicate how long the threads must go to sleep 
        //the thing is, if you set it low, you will notice that threads may not exactly alternate
        //the best way to understand what I am saying is to set this lower, like say 0
        //and then, change it to 10 or 100 or any value you like

        //I am using the thread.sleep method to indicate that the current thread should go to sleep
        //this allows the other thread to takeover. Then, the second thread will go to sleep
        //the original thread will take over. 
        public static int sleep_duration = 1000;

        static void Main(string[] args)
        {
            //let me create a new thread
            //to this thread, I am sending the method from below as the parameter
            Thread t = new Thread(new ThreadStart(thread_demo));

            //starting the thread
            t.Start();

            //a simple loop in main
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Main - this is from the loop. i is {0}", i);
                //stop the current thread
                Thread.Sleep(sleep_duration);
            }

            //this is to indicate that the thread t has to wait till the main thread has stopped
            t.Join();

            //dont let the console window dissapear
            Console.ReadLine();
        }

        //method that will be called by the second thread
        public static void thread_demo()
        {
            //a simple loop
            for(int i=0;i<10;i++)
            {
                Console.WriteLine("thread_demo - this is from the loop. i is {0}", i);
                //stop the current thread
                Thread.Sleep(sleep_duration);
            }
        }
    }
}
